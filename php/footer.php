			<div class="row dark footer-row full-width padding-top-30">
				<div class="row padding-bottom-33">
					<div class="column column-1-3">
						<ul class="contact-details-list">
							<li class="features-phone">
								<label>CHIAMACI ORA</label>
								<p><a href="tel:+393358368978">+39 335 8368978</a></p>
							</li>
						</ul>
					</div>
					<div class="column column-1-3">
						<ul class="contact-details-list">
							<li class="features-map">
								<label>DOVE SIAMO</label>
								<p>Forlì, Italia</p>
							</li>
						</ul>
					</div>
					<div class="column column-1-3">
						<ul class="contact-details-list">
							<li class="features-wallet">
								<label>DOMANDE?</label>
								<p><a href="?page=service_calculator" title="Online Service Calculator">Richiedi un preventivo</a></p>
							</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="row dark-gray footer-row full-width padding-top-61 padding-bottom-36">
				<div class="row row-4-4">
					<div class="column column-1-4">
						<h6>CHI SIAMO</h6>
						<p class="margin-top-23">Da oltre 25 anni, la nostra impresa di pulizie è specializzata in servizi di pulizia e manutenzione di ambienti civili ed industriali.</p>
						<p>Ditta Sole intende creare un dialogo diretto e continuativo con i propri clienti, ponendosi come unico interlocutore per fornire svariati servizi nell’ambito della pulizia.</p>
						<div class="margin-top-37 padding-bottom-16">
							<a class="more gray" href="?page=contact_3" title="Contact Style 3">Contattaci</a>
						</div>
					</div>
					<div class="column column-1-4">
						<h6>SERVIZI</h6>
						<ul class="list margin-top-31">
							<li class="template-arrow-horizontal-2"><a href="?page=service_house_cleaning" title="Commercial Cleaning">Pulizia di Uffici</a></li>
							<li class="template-arrow-horizontal-2"><a href="?page=service_post_renovation" title="House Cleaning">Pulizia di Appartamenti</a></li>
							<li class="template-arrow-horizontal-2"><a href="?page=service_green_spaces_maintenance" title="Move In Out Service">Ripristino Pavimentazione</a></li>
							<li class="template-arrow-horizontal-2"><a href="?page=service_move_in_out" title="Post Renovation">Lavaggio di Moquette</a></li>
							<li class="template-arrow-horizontal-2"><a href="?page=service_commercial_cleaning" title="Window Cleaning">Lavaggio Divani</a></li>
							<li class="template-arrow-horizontal-2"><a href="?page=service_window_cleaning" title="Green Spaces Maintenance">Lavaggio Vetrate</a></li>
							<li class="template-arrow-horizontal-2"><a href="?page=service_pulizia_palestre" title="Pulizia Palestre">Pulizia Palestre</a></li>
							<li class="template-arrow-horizontal-2"><a href="?page=service_pulizia_negozi" title="Pulizia Negozi">Pulizia Negozi</a></li>
						</ul>
					</div>
					<div class="column column-1-4">
						<h6>PREVENTIVI</h6>
						<p class="margin-top-23">Potete richiedere un preventivo in maniera rapida e senza alcun impegno nell'apposita sezione accessibile tramite il pulsante sottostante.</p>
						<p>Inserite la metratura ed il numero di locali dello stabile che intendete trattare e riceverete nel più breve tempo possibile un dettagliato preventivo sui servizi offerti da ditta Sole.</p>
						<div class="margin-top-37 padding-bottom-16">
							<a class="more gray" href="?page=service_calculator" title="Service calculator">Richiedi un Preventivo</a>
						</div>
					</div>
					<div class="column column-1-4">
						<h6>CONTATTI</h6>
						<ul class="contact-data margin-top-20">
							<li class="template-location"><div class="value">Forlì (FC), Italia</div></li>
							<li class="template-mobile"><div class="value"><a href="tel:+393549563776">+39 354 9563776</a></div></li>
							<li class="template-email"><div class="value"><a href="mailto:a.solepulizie@gmail.com">a.solepulizie@gmail.com</a></div></li>
							<li class="template-clock"><div class="value">Aperti tutti i giorni</div></li>
						</ul>
					</div>
				</div>
				<div class="row page-padding-top">
					<ul class="social-icons align-center">
						<li>
							<a target="_blank" href="#" class="social-twitter" title="twitter"></a>
						</li>
						<li>
							<a href="#" class="social-pinterest" title="pinterest"></a>
						</li>
						<li>
							<a target="_blank" href="#" class="social-facebook" title="facebook"></a>
						</li>
					</ul>
				</div>
				<div class="row align-center padding-top-30">
					<span class="copyright">© Copyright 2021 <a href="#" title="Cleanmate Template" target="_blank">Ditta Sole</a> by <a href="http://www.homelineagency.com" title="QuanticaLabs" target="_blank">HomeLine Agency</a></span>
				</div>
			</div>
		</div>
		<a href="#top" class="scroll-top animated-element template-arrow-vertical-3" title="Scroll to top"></a>
		<div class="background-overlay"></div>
		<!--js-->
		<script type="text/javascript" src="js/jquery-1.12.4.min.js"></script>
		<script type="text/javascript" src="js/jquery-migrate-1.4.1.min.js"></script>
		<!--slider revolution-->
		<script type="text/javascript" src="rs-plugin/js/jquery.themepunch.tools.min.js"></script>
		<script type="text/javascript" src="rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
		<script type="text/javascript" src="js/jquery.ba-bbq.min.js"></script>
		<script type="text/javascript" src="js/jquery-ui-1.12.1.custom.min.js"></script>
		<script type="text/javascript" src="js/jquery.ui.touch-punch.min.js"></script>
		<script type="text/javascript" src="js/jquery.isotope.min.js"></script>
		<script type="text/javascript" src="js/jquery.easing.1.3.min.js"></script>
		<script type="text/javascript" src="js/jquery.carouFredSel-6.2.1-packed.js"></script>
		<script type="text/javascript" src="js/jquery.touchSwipe.min.js"></script>
		<script type="text/javascript" src="js/jquery.transit.min.js"></script>
		<script type="text/javascript" src="js/jquery.timeago.js"></script>
		<script type="text/javascript" src="js/jquery.hint.min.js"></script>
		<script type="text/javascript" src="js/jquery.costCalculator.min.js"></script>
		<script type="text/javascript" src="js/jquery.parallax.min.js"></script>
		<script type="text/javascript" src="js/jquery.prettyPhoto.js"></script>
		<script type="text/javascript" src="js/jquery.qtip.min.js"></script>
		<script type="text/javascript" src="js/jquery.blockUI.min.js"></script>
		<?php if($_GET["page"]=="contact" || $_GET["page"]=="contact_2" || $_GET["page"]=="contact_3"):?>
		<script type="text/javascript" src="//maps.google.com/maps/api/js?key=YOUR_API_KEY"></script>
		<?php endif; ?>
		<script type="text/javascript" src="js/main.js"></script>
		<script type="text/javascript" src="js/odometer.min.js"></script>
		<?php
		require_once("style_selector/style_selector.php");
		?>
	</body>
</html>