<?php
ob_start();
?>
<html>
	<head>
	</head>
	<body>
		<div><b>Nome</b>: <?php echo $values["name"]; ?></div>
		<div><b>E-mail</b>: <?php echo $values["email"]; ?></div>
		<?php if($values["phone"]!=""):?>
		<div><b>Telefono</b>: <?php echo $values["phone"]; ?></div>
		<?php endif; ?>
		<?php if($values["message"]!=""):?>
		<div><b>Messaggio</b>: <?php echo nl2br($values["message"]); ?></div>
		<?php endif; ?>
		<?php if($values["message_calculator"]!=""):?>
		<div><b>Messaggio</b>: <?php echo nl2br($values["message_calculator"]); ?></div>
		<?php endif; ?>
		<?php echo $form_data; ?>
	</body>
</html>
<?php
$content = ob_get_contents();
ob_end_clean();
return($content);
?>	