</div>
<!-- Slider Revolution -->
<div class="revolution-slider-container">
	<div class="revolution-slider" data-version="5.4.5">
		<ul>
			<!-- SLIDE 1 -->
			<li data-transition="fade" data-masterspeed="500" data-slotamount="1" data-delay="6000">
				<div class="slider1">
					<!-- LAYERS -->
					<!-- LAYER 01 -->
					<div class="tp-caption"
						data-frames='[{"delay":500,"speed":1500,"from":"y:-40;o:0;","ease":"easeInOutExpo"},{"delay":"wait","speed":500,"to":"o:0;","ease":"easeInOutExpo"}]'
						data-x="center"
						data-y="['211', '257', '160', '218']"
						>
						<!--h4>User-Friendly. Simple. Awesome.</h4-->
					</div>
					<!-- LAYER 02 -->
					<div class="tp-caption"
						data-frames='[{"delay":500,"speed":2000,"from":"y:40;o:0;","ease":"easeInOutExpo"},{"delay":"wait","speed":500,"to":"o:0;","ease":"easeInOutExpo"}]'
						data-x="center"
						data-y="['273', '313', '200', '260']"
						>
						<h2><a href="?page=service_calculator" title="Estimate Total Costs">LA TUA SCELTA</a></h2>
					</div>
					<!-- LAYER 03 -->
					<div class="tp-caption"
						data-frames='[{"delay":900,"speed":2000,"from":"y:40;o:0;","ease":"easeInOutExpo"},{"delay":"wait","speed":500,"to":"o:0;","ease":"easeInOutExpo"}]'
						data-x="center"
						data-y="['345', '368', '236', '290']"
						>
						<h2 class="slider-subtitle"><strong>CHE FA LA DIFFERENZA</strong></h2>
					</div>
					<!-- LAYER 04 -->
					<div class="tp-caption"
						data-frames='[{"delay":1100,"speed":1500,"from":"y:40;o:0;","ease":"easeInOutExpo"},{"delay":"wait","speed":500,"to":"o:0;","ease":"easeInOutExpo"}]'
						data-x="center"
						data-y="['476', '478', '230', '230']"
						>					
						<div class="align-center">
							<a class="more1" href="?page=service_calculator" title="Service calculator">Richiedi un Preventivo</a>
						</div>
					</div>
					<!-- / -->
				</div>
			</li>
			<!-- SLIDE 2 -->
			<li data-transition="fade" data-masterspeed="500" data-slotamount="1" data-delay="6000">
				<div class="slider2">
					<!-- LAYERS -->
					<!-- LAYER 01 -->
					<div class="tp-caption customin customout"
						data-frames='[{"delay":500,"speed":1500,"from":"y:-40;o:0;","ease":"easeInOutExpo"},{"delay":"wait","speed":500,"to":"o:0;","ease":"easeInOutExpo"}]'
						data-x="center"
						data-y="['211', '257', '160', '218']"
						>
						<!--h4>Reliable and Stable Crews</h4-->
					</div>
					<!-- LAYER 02 -->
					<div class="tp-caption customin customout"
						data-frames='[{"delay":500,"speed":2000,"from":"y:40;o:0;","ease":"easeInOutExpo"},{"delay":"wait","speed":500,"to":"o:0;","ease":"easeInOutExpo"}]'
						data-x="center"
						data-y="['273', '313', '200', '260']"
						>
						<h2><a href="?page=service_calculator" title="Estimate Total Costs">IL NOSTRO TEAM</a></h2>
					</div>
					<!-- LAYER 03 -->
					<div class="tp-caption customin customout"
						data-frames='[{"delay":900,"speed":2000,"from":"y:40;o:0;","ease":"easeInOutExpo"},{"delay":"wait","speed":500,"to":"o:0;","ease":"easeInOutExpo"}]'
						data-x="center"
						data-y="['345', '368', '236', '290']"
						>
						<h2 class="slider-subtitle"><strong>A TUA DISPOSIZIONE</strong></h2>
					</div>
					<!-- LAYER 04 -->
					<div class="tp-caption customin customout"
						data-frames='[{"delay":1100,"speed":1500,"from":"y:40;o:0;","ease":"easeInOutExpo"},{"delay":"wait","speed":500,"to":"o:0;","ease":"easeInOutExpo"}]'
						data-x="center"
						data-y="['476', '478', '230', '230']"
						>
						<div class="align-center">
							<a class="more1" href="?page=service_calculator" title="Service calculator">Richiedi un Preventivo</a>
						</div>
					</div>
					<!-- / -->
				</div>
			</li>
			
			<!-- SLIDE 3 -->
			<li data-transition="fade" data-masterspeed="500" data-slotamount="1" data-delay="6000">
				<div class="slider3">
					<!-- LAYERS -->
					<!-- LAYER 01 -->
					<div class="tp-caption customin customout"
						data-frames='[{"delay":500,"speed":1500,"from":"y:-40;o:0;","ease":"easeInOutExpo"},{"delay":"wait","speed":500,"to":"o:0;","ease":"easeInOutExpo"}]'
						data-x="center"
						data-y="['211', '257', '160', '218']"
						>
						<!--h4>Our Focus is to Really Listen to Our Clients</h4-->
					</div>
					<!-- LAYER 02 -->
					<div class="tp-caption customin customout"
						data-frames='[{"delay":500,"speed":2000,"from":"y:40;o:0;","ease":"easeInOutExpo"},{"delay":"wait","speed":500,"to":"o:0;","ease":"easeInOutExpo"}]'
						data-x="center"
						data-y="['273', '313', '200', '260']"
						>
						<h2><a href="?page=service_calculator" title="Estimate Total Costs">IL PREVENTIVO IDEALE</a></h2>
					</div>
					<!-- LAYER 03 -->
					<div class="tp-caption customin customout"
						data-frames='[{"delay":900,"speed":2000,"from":"y:40;o:0;","ease":"easeInOutExpo"},{"delay":"wait","speed":500,"to":"o:0;","ease":"easeInOutExpo"}]'
						data-x="center"
						data-y="['345', '368', '236', '290']"
						>
						<h2 class="slider-subtitle"><strong>A PORTATA DI UN CLICK</strong></h2>
					</div>
					<!-- LAYER 04 -->
					<div class="tp-caption customin customout"
						data-frames='[{"delay":1100,"speed":1500,"from":"y:40;o:0;","ease":"easeInOutExpo"},{"delay":"wait","speed":500,"to":"o:0;","ease":"easeInOutExpo"}]'
						data-x="center"
						data-y="['476', '478', '230', '230']"
						>
						<div class="align-center">
							<a class="more1" href="?page=service_calculator" title="Service calculator">Richiedi un Preventivo</a>
						</div>
					</div>
					<!-- / -->
				</div>
			</li>
		</ul>
	</div>
</div>
<!--/-->
<div class="theme-page">
	<div class="clearfix" style="margin-bottom: 50px;">
		<div class="row margin-top-89">
			<div class="row">
				<h2 class="box-header">AFFIDATI A NOI!</h2>
				<p class="description align-center">Da oltre 25 anni, la nostra impresa di pulizie è specializzata in servizi<br>di pulizia e manutenzione di ambienti civili ed industriali.</p>
				<div class="row page-margin-top">
					<div class="column column-1-4">
						<ul class="features-list align-right margin-top-30">
							<li>
								<div class="icon features-diamond"></div>
								<h4>PULIZIA SPLENDENTE</h4>
								<p>Noi di ditta Sole, grazie ad anni di esperienza, sappiamo come far risplendere i tuoi uffici ed edifici lavorativi.</p>
							</li>
							<li>
								<div class="icon features-umbrella"></div>
								<h4>ADATTAMENTO</h4>
								<p>Operiamo in qualsiasi condizione, la soddisfazione del cliente è sempre la nostra priorità.</p>
							</li>
						</ul>
					</div>
					<div class="column column-1-2 align-center">
						<div class="image-wrapper">
							<img src="images/samples/480x480/cerchio.jpg" alt="" class="radius border">
						</div>
					</div>
					<div class="column column-1-4">
						<ul class="features-list margin-top-30">
							<li>
								<div class="icon features-eco"></div>
								<h4>STRUMENTI ADEGUATI</h4>
								<p>Il segreto di un buon pulito sta anche nei prodotti scelti, e Ditta Sole non lascia niente al caso.</p>
							</li>
							<li>
								<div class="icon features-maid"></div>
								<h4>UN TEAM ECCELLENTE</h4>
								<p>I nostri addetti sanno quanto sono importanti l’igiene e la profonda pulizia per un lavoro impeccabile.</p>
							</li>
						</ul>
					</div>
				</div>
				<div class="align-center margin-top-65 padding-bottom-16">
					<a class="more" href="?page=contact_3" title="Contact Style 3">Contattaci</a>
				</div>
			</div>	
		</div>
		<div class="row full-width padding-top-89 padding-bottom-96 page-margin-top-section parallax parallax-1 overlay">
			<div class="row">
				<h2 class="box-header white">QUALITÀ, VERSATILITÀ, RAPIDITÀ</h2>
				<p class="description align-center white">I tre sostantivi che negli anni ci hanno permesso di soddisfare centinaia di clienti e di far crescere ininterrottamente la nostra azienda.</p>
				<div class="tabs white no-scroll margin-top-27 clearfix">
					<ul class="tabs-navigation clearfix">
						<li>
							<a href="#our-customers" title="Our Customers" class="features-team">
								QUALITÀ
							</a>
						</li>
						<li>
							<a href="#the-environment" title="The Environment" class="features-leaf">
								VERSATILITÀ
							</a>
						</li>
						<li>
							<a href="#communication" title="Communication" class="features-megaphone">
								RAPIDITÀ
							</a>
						</li>
					</ul>
					<div id="our-customers">
						<p>Precisione e cura assoluta di ogni anche minimo dettaglio. Per ditta Sole sono i particolari a fare la differenza. Ogni lavoro lo affrontiamo tentando di raggiungere la perfezione assoluta senza nessuna eccezione: vietato trascurare anche il minimo dettaglio.</p>
					</div>
					<div id="the-environment">
						<p>Anni di esperienza e la grande competenza di tutti i membri di ditta Sole, ci hanno permesso di saper affrontare in maniera specifica ogni tipo di spazio, indipendentemente dalla morfologia dell’edificio, dalla grandezza e dalla tipologia di pavimentazione.</p>
					</div>
					<div id="communication">
						<p>Sappiamo quanto sia importante per il cliente poter disporre dei propri spazi nel più breve tempo possibile. Ditta Sole opera sempre nel rispetto del tempo del cliente, ma senza mai trascurare la qualità e la precisione che, da sempre, ci contraddistinguono.</p>
					</div>
				</div>
			</div>
		</div>
		<div class="row full-width gray padding-top-89 padding-bottom-96">
			<div class="row">
				<h2 class="box-header">I NOSTRI SERVIZI</h2>
				<p class="description align-center">La tua scelta che fa la differenza.</p>
				<div class="carousel-container margin-top-65 clearfix">
					<ul class="services-list horizontal-carousel clearfix page-margin-top">
						<li class="column column-1-3">
							<a href="?page=service_house_cleaning" title="House Cleaning">
								<img src="images/ufficio.jpg" alt="">
							</a>
							<h4 class="box-header"><a href="?page=service_house_cleaning" title="House Cleaning">PULIZIA PERIODICA DI UFFICI</a></h4>
							<p>Offriamo singoli, periodici o continuativi servizi di sanificazione all’interno di uffici privati e pubblici o generici ambienti lavorativi.</p>
						</li>
						<li class="column column-1-3">
							<a href="?page=service_post_renovation" title="Post Renovation">
								<img src="images/appartamento.jpg" alt="">
							</a>
							<h4 class="box-header"><a href="?page=service_post_renovation" title="Post Renovation">PULIZIA DI APPARTAMENTI</a></h4>
							<p>Garantiamo sempre un accurato servizio di ripristino o mantenimento all’interno di ogni tipo di appartamento privato.</p>
						</li>
						<li class="column column-1-3">
							<a href="?page=service_green_spaces_maintenance" title="Green Spaces Maintenance">
								<img src="images/pavimento.jpg" alt="">
							</a>
							<h4 class="box-header"><a href="?page=service_green_spaces_maintenance" title="Green Spaces Maintenance">RIPRISTINO PAVIMENTAZIONE</a></h4>
							<p>Uno dei servizi più apprezzati offerti dalla nostra ditta. Trattiamo ogni tipo di superficie: dal cotto al marmo, fino al parquet.</p>
						</li>
						<li class="column column-1-3">
							<a href="?page=service_move_in_out" title="Move In Out Service">
								<img src="images/moquette.jpg" alt="">
							</a>
							<h4 class="box-header"><a href="?page=service_move_in_out" title="Move In Out Service">LAVAGGIO DI MOQUETTE</a></h4>
							<p>Utilizziamo protocolli specifici e macchinari ad iniezione e aspirazione per rendere la tua moquette come il primo giorno.</p>
						</li>
						<li class="column column-1-3">
							<a href="?page=service_commercial_cleaning" title="Commercial Cleaning">
								<img src="images/divano.jpg" alt="">
							</a>
							<h4 class="box-header"><a href="?page=service_commercial_cleaning" title="Commercial Cleaning">LAVAGGIO INTEGRALE DIVANI</a></h4>
							<p>Rivitalizziamo i tessuti con macchinari specifici ed altamente professionali, adatti alle aree non sfoderabili ed imbottite.</p>
						</li>
						<li class="column column-1-3">
							<a href="?page=service_window_cleaning" title="Window Cleaning">
								<img src="images/vetrata.jpg" alt="">
							</a>
							<h4 class="box-header"><a href="?page=service_window_cleaning" title="Window Cleaning">LAVAGGIO INTEGRALE VETRATE</a></h4>
							<p>Disponiamo dei prodotti migliori per il trattamento completo dei vetri ed eliminiamo lo sporco più ostinato con olio di gomito.</p>
						</li>
						<li class="column column-1-3">
							<a href="?page=service_pulizia_palestre" title="Pulizia Palestre">
								<img src="images/palestra.jpg" alt="">
							</a>
							<h4 class="box-header"><a href="?page=service_pulizia_palestre" title="Pulizia Palestre">PULIZIA COMPLETA PALESTRE</a></h4>
							<p>Ci occupiamo della pulizia di ogni ambiente della palestra, nel rispetto delle condizioni igienico-sanitarie richieste da tali spazi.</p>
						</li>
						<li class="column column-1-3">
							<a href="?page=service_pulizia_negozi" title="Pulizia Negozi">
								<img src="images/negozio.jpg" alt="">
							</a>
							<h4 class="box-header"><a href="?page=service_pulizia_negozi" title="Pulizia Negozi">PULIZIA COMPLETA NEGOZI</a></h4>
							<p>Curiamo l’immagine della tua realtà al fine di assicurare la buona rendita della tua attività e la fidelizzazione della tua clientela.</p>
						</li>
					</ul>
					<div class="cm-carousel-pagination"></div>
				</div>
			</div>
		</div>
		<div class="row full-width padding-top-112 padding-bottom-100 page-margin-top-section counters-group parallax parallax-2 overlay">
			<div class="row">
				<div class="column column-1-4">
					<div class="counter-box padding-cambiato">
						<div class="ornament-container">
							<div class="ornament animated-element duration-2000 animation-ornamentHeight" data-animation-start="230"></div>
						</div>
						<span class="number animated-element" data-value="295" data-animation-start="230"></span>
						<p>TRASFERTE COMPLETATE</p>
					</div>
				</div>
				<div class="column column-1-4">
					<div class="counter-box padding-cambiato">
						<div class="ornament-container">
							<div class="ornament animated-element duration-2000 animation-ornamentHeight" data-animation-start="230"></div>
						</div>
						<span class="number animated-element" data-value="400" data-animation-start="230"></span>
						<p>CLIENTI SODDISFATTI</p>
					</div>
				</div>
				<div class="column column-1-4">
					<div class="counter-box padding-cambiato">
						<div class="ornament-container">
							<div class="ornament animated-element duration-2000 animation-ornamentHeight" data-animation-start="230"></div>
						</div>
						<span class="number animated-element" data-value="527" data-animation-start="230"></span>
						<p>PROGETTI COMPLETATI</p>
					</div>
				</div>
				<div class="column column-1-4">
					<div class="counter-box padding-cambiato">
						<div class="ornament-container">
							<div class="ornament animated-element duration-2000 animation-ornamentHeight" data-animation-start="230"></div>
						</div>
						<span class="number animated-element" data-value="105" data-animation-start="230"></span>
						<p>LUOGHI PULITI</p>
					</div>
				</div>
			</div>
		</div>
		<div class="row page-margin-top-section">
			<!--div class="column column-1-4">
				<div class="row">
					<a href="images/samples/480x320/placeholder.jpg" class="prettyPhoto cm-preload" title="House Cleaning">
						<img src='images/samples/480x320/placeholder.jpg' alt='img'>
					</a>
				</div>
				<div class="row margin-top-30">
					<a href="images/chi-siamo.jpg" class="prettyPhoto cm-preload" title="Window Cleaning">
						<img src='images/chi-siamo.jpg' alt='img'>
					</a>
				</div>
			</div-->
			<div class="column column-1-2">
				<a href="images/chi-siamo.jpg" class="prettyPhoto cm-preload" title="Chi Siamo">
					<img src='images/chi-siamo.jpg' alt='img'>
				</a>
			</div>
			<div class="column column-1-2">
				<h2 class="box-header">CHI SIAMO</h2>
				<p class="description align-center">Da oltre 25 anni, la nostra impresa di pulizie è specializzata in servizi di pulizia e manutenzione di ambienti civili ed industriali.</p>
				<p class="align-center padding-0 margin-top-27 padding-left-right-35">Operando nell’intero territorio regionale e grazie all’esperienza maturata nel settore, è nostra premura offrire un’organizzazione efficiente ed elastica, massimizzando il rapporto tra tempo impiegato e risultato finale, sia in termini di rapidità che di qualità. Ditta Sole intende creare un dialogo diretto e continuativo con i propri clienti, ponendosi come unico interlocutore per fornire svariati servizi nell’ambito della pulizia.</p>
				<div class="align-center page-margin-top padding-bottom-16">
					<a class="more" href="?page=contact_3" title="Contact Style 3">Contattaci</a>
				</div>
			</div>
		</div>

		<!--div class="row full-width padding-top-89 padding-bottom-100">
			<div class="row">
				<h2 class="box-header">SELECTED PROJECTS</h2>
				<p class="description align-center">Explore completed projects.</p>
				<div class="carousel-container margin-top-65 clearfix">
					<ul class="projects-list horizontal-carousel clearfix page-margin-top">
						<li class="column column-1-3">
							<a href="?page=project_apartment_cleaning" title="Apartment Cleaning">
								<img src="images/samples/480x320/placeholder.jpg" alt="">
							</a>
							<div class="view align-center">
								<div class="vertical-align-table">
									<div class="vertical-align-cell">
										<p>Apartment cleaning</p>
										<a class="more simple" href="?page=project_apartment_cleaning" title="View project">View project</a>
									</div>
								</div>
							</div>
						</li>
						<li class="column column-1-3">
							<a href="?page=project_gutter_cleaning" title="Gutter Cleaning">
								<img src="images/samples/480x320/placeholder.jpg" alt="">
							</a>
							<div class="view align-center">
								<div class="vertical-align-table">
									<div class="vertical-align-cell">
										<p>Gutter Cleaning</p>
										<a class="more simple" href="?page=project_gutter_cleaning" title="View project">View project</a>
									</div>
								</div>
							</div>
						</li>
						<li class="column column-1-3">
							<a href="?page=project_move_in_out" title="Move In Out">
								<img src="images/samples/480x320/placeholder.jpg" alt="">
							</a>
							<div class="view align-center">
								<div class="vertical-align-table">
									<div class="vertical-align-cell">
										<p>Move In Out</p>
										<a class="more simple" href="?page=project_move_in_out" title="View project">View project</a>
									</div>
								</div>
							</div>
						</li>
						<li class="column column-1-3">
							<a href="?page=project_after_renovation_cleaning" title="After Renovation Cleaning">
								<img src="images/samples/480x320/placeholder.jpg" alt="">
							</a>
							<div class="view align-center">
								<div class="vertical-align-table">
									<div class="vertical-align-cell">
										<p>After Renovation Cleaning</p>
										<a class="more simple" href="?page=project_after_renovation_cleaning" title="View project">View project</a>
									</div>
								</div>
							</div>
						</li>
						<li class="column column-1-3">
							<a href="?page=project_house_cleaning" title="House Cleaning">
								<img src="images/samples/480x320/placeholder.jpg" alt="">
							</a>
							<div class="view align-center">
								<div class="vertical-align-table">
									<div class="vertical-align-cell">
										<p>House Cleaning</p>
										<a class="more simple" href="?page=project_house_cleaning" title="View project">View project</a>
									</div>
								</div>
							</div>
						</li>
						<li class="column column-1-3">
							<a href="?page=project_garden_maintenance" title="Garden Maintenance">
								<img src="images/samples/480x320/placeholder.jpg" alt="">
							</a>
							<div class="view align-center">
								<div class="vertical-align-table">
									<div class="vertical-align-cell">
										<p>Garden Maintenance</p>
										<a class="more simple" href="?page=project_garden_maintenance" title="View project">View project</a>
									</div>
								</div>
							</div>
						</li>
						<li class="column column-1-3">
							<a href="?page=project_apartment_cleaning" title="Apartment Cleaning">
								<img src="images/samples/480x320/placeholder.jpg" alt="">
							</a>
							<div class="view align-center">
								<div class="vertical-align-table">
									<div class="vertical-align-cell">
										<p>Apartment cleaning</p>
										<a class="more simple" href="?page=project_apartment_cleaning" title="View project">View project</a>
									</div>
								</div>
							</div>
						</li>
						<li class="column column-1-3">
							<a href="?page=project_gutter_cleaning" title="Gutter Cleaning">
								<img src="images/samples/480x320/placeholder.jpg" alt="">
							</a>
							<div class="view align-center">
								<div class="vertical-align-table">
									<div class="vertical-align-cell">
										<p>Gutter Cleaning</p>
										<a class="more simple" href="?page=project_gutter_cleaning" title="View project">View project</a>
									</div>
								</div>
							</div>
						</li>
						<li class="column column-1-3">
							<a href="?page=project_move_in_out" title="Move In Out">
								<img src="images/samples/480x320/placeholder.jpg" alt="">
							</a>
							<div class="view align-center">
								<div class="vertical-align-table">
									<div class="vertical-align-cell">
										<p>Move In Out</p>
										<a class="more simple" href="?page=project_move_in_out" title="View project">View project</a>
									</div>
								</div>
							</div>
						</li>
					</ul>
					<div class="cm-carousel-pagination"></div>
				</div>
			</div>
		</div-->
	</div>
</div>