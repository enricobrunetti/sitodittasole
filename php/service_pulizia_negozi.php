<div class="theme-page padding-bottom-100">
	<div class="row gray full-width page-header vertical-align-table">
		<div class="row">
			<div class="page-header-left">
				<h1>PULIZIA NEGOZI</h1>
			</div>
			<div class="page-header-right">
				<div class="bread-crumb-container">
					<ul class="bread-crumb">
						<li>
							<a title="Home" href="?page=home">
								Home
							</a>
						</li>
						<li class="separator">
							&#47;
						</li>
						<li>
							<a title="Our Services" href="?page=services">
								Servizi
							</a>
						</li>
						<li class="separator">
							&#47;
						</li>
						<li>
							Pulizia Negozi
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<div class="clearfix">
		<div class="row page-margin-top-section">
			<div class="column column-1-4">
				<ul class="vertical-menu">
					<li>
						<a href="?page=service_house_cleaning" title="House Cleaning">
							Pulizia di Uffici
							<span class="template-arrow-horizontal-3"></span>
						</a>
					</li>
					<li>
						<a href="?page=service_post_renovation" title="Post Renovation">
							Pulizia di Appartamenti
							<span class="template-arrow-horizontal-3"></span>
						</a>
					</li>
					<li>
						<a href="?page=service_green_spaces_maintenance" title="Green Spaces Maintenance">
							Ripristino Pavimentazione
							<span class="template-arrow-horizontal-3"></span>
						</a>
					</li>
					<li>
						<a href="?page=service_move_in_out" title="Move In Out Service">
							Lavaggio Moquette
							<span class="template-arrow-horizontal-3"></span>
						</a>
					</li>
					<li>
						<a href="?page=service_commercial_cleaning" title="Commercial Cleaning">
							Lavaggio Divani
							<span class="template-arrow-horizontal-3"></span>
						</a>
					</li>
					<li>
						<a href="?page=service_window_cleaning" title="Window Cleaning">
							Lavaggio Vetrate
							<span class="template-arrow-horizontal-3"></span>
						</a>
					</li>
                    <li>
						<a href="?page=service_pulizia_palestre" title="Pulizia Palestre">
							Pulizia Palestre
							<span class="template-arrow-horizontal-3"></span>
						</a>
					</li>
                    <li class="selected">
						<a href="?page=service_pulizia_negozi" title="Pulizia Negozi">
							Pulizia Negozi
							<span class="template-arrow-horizontal-3"></span>
						</a>
					</li>
				</ul>
				<ul class="services-list services-icons gray page-margin-top clearfix">
					<li>
						<a href="?page=service_calculator" title="Cost Calculator">
							<span class="service-icon features-calculator"></span>
						</a>
						<h4><a href="?page=service_calculator" title="Cost Calculator">PREVENTIVI</a></h4>
						<p class="description">Richiedi un preventivo.</p>
						<div class="align-center margin-top-40 padding-bottom-16">
							<a class="more" href="?page=service_calculator" title="Learn more">Contattaci</a>
						</div>
					</li>
				</ul>
				<!--h6 class="box-header page-margin-top">DOWNLOAD</h6>
				<ul class="buttons margin-top-30">
					<li class="template-arrow-vertical-3">
						<a href="#" title="Brochure">Brochure</a>
					</li>
					<li class="template-arrow-vertical-3">
						<a href="#" title="Summary">Summary</a>
					</li>
				</ul-->
			</div>
			<div class="column column-3-4">
				<div class="row">
					<div class="column column-1-2">
						<a href="images/negozio.jpg" class="prettyPhoto cm-preload" title="Pulizia Negozi">
							<img src='images/negozio.jpg' alt='img'>
						</a>
					</div>
					<div class="column column-1-2">
						<a href="images/negozio2.jpg" class="prettyPhoto cm-preload" title="Pulizia Negozi">
							<img src='images/negozio2.jpg' alt='img'>
						</a>
					</div>
				</div>
				<div class="row page-margin-top">
					<div class="column-1-1">
						<h3>PANORAMICA</h3>
						<p class="description">Curiamo l’immagine della tua realtà al fine di assicurare la buona rendita della tua attività e la fidelizzazione della tua clientela.</p>
					</div>
				</div>
				<div class="row page-margin-top">
					<div class="column column-1-2">
						<h4>PULIZIA NEGOZI</h4>
						<p class="margin-top-14">Un ambiente pulito è il primo biglietto da visita di un’attività commerciale. Dal piccolo negozio al grande stabile, la ditta Sole saprà come rendere impeccabile ogni dettaglio del tuo esercizio. Grazie all’esperienza e professionalità dei nostri operatori, ditta Sole sa come trattare gli spazi della tua attività.</p>
						<p>Con cura e attenzione, agiamo con massima competenza in ogni tipologia di locale, indipendentemente dalla morfologia e dalla grandezza, sempre mediante l’utilizzo dei migliori prodotti.</p>
					</div>
					<div class="column column-1-2">
						<h4>SERVIZI</h4>
						<!--p class="margin-top-14">Founded in 1995 we became one of the leading providers of residential and commercial cleaning solutions in Canada and United States. Our mission is to:</p-->
						<ul class="list margin-top-10">
							<li class="template-tick-1">Lavaggio e sanificazione pavimenti.</li>
							<li class="template-tick-1">Spolveratura arredi.</li>
							<li class="template-tick-1">Sanificazione telefoni, tastiere e mouse.</li>
							<li class="template-tick-1">Pulizia e igienizzazione maniglie.</li>
							<li class="template-tick-1">Svuotamento e igienizzazione cestini.</li>
                            <li class="template-tick-1">Pulizia pareti attrezzate e separé.</li>
                            <li class="template-tick-1">Pulizia vetrate e finestre.</li>
                            <li class="template-tick-1">Pulizia e sanificazione wc.</li>
						</ul>
					</div>
				</div>
				<div class="divider page-margin-top"></div>
				<div class="row margin-top-30">
					<ul class="services-list services-icons clearfix">
						<li class="column column-1-3">
							<span class="service-icon big features-credit-card tick"></span>
							<h4>PAGAMENTI FACILI</h4>
							<p>Il cliente è facilitato dagli innumerevoli metodi di pagamento disponibili.</p>
						</li>
						<li class="column column-1-3">
							<span class="service-icon big features-pet tick"></span>
							<h4>AMICI DEGLI ANIMALI</h4>
							<p>Il nostro straff tratterà i tuoi animali come se fossero membri della famiglia.</p>
						</li>
						<li class="column column-1-3">
							<span class="service-icon big features-delivery tick"></span>
							<h4>SEMPRE IN ORARIO</h4>
							<p>Il tuo tempo è importante per noi. Forniamo servizi di pulizia sempre puntuali.</p>
						</li>
					</ul>
				</div>
				<div class="divider margin-top-27"></div>
				<div class="row page-margin-top">
					<!--div class="column column-1-2">
						<h3>POPULAR QUESTIONS</h3>
						<ul class="accordion margin-top-40 clearfix">
							<li>
								<div id="accordion-house-cleaning">
									<h4>How many people will come to clean my house?</h4>
								</div>
								<div class="clearfix">
									<p>What makes Cleanmate trusted above other cleaning service providers? When you combine higher standards, smarter strategies and superior quality all in one package, the result is top notch.</p>
								</div>
							</li>
							<li>
								<div id="accordion-present-for-cleaning">
									<h4>Do I have to be present for a cleaning?</h4>
								</div>
								<div class="clearfix">
									<p>What makes Cleanmate trusted above other cleaning service providers? When you combine higher standards, smarter strategies and superior quality all in one package, the result is top notch.</p>
								</div>
							</li>
							<li>
								<div id="accordion-how-long">
									<h4>How long will it take to clean my house?</h4>
								</div>
								<div class="clearfix">
									<p>What makes Cleanmate trusted above other cleaning service providers? When you combine higher standards, smarter strategies and superior quality all in one package, the result is top notch.</p>
								</div>
							</li>
						</ul>
					</div-->
					<!--div class="column column-1-2">
						<h3>PLAN PRICING</h3>
						<table class="margin-top-40 gray-first">
							<tbody>
								<tr>
									<td>Carpet cleaning</td>
									<td>$25 - $150</td>
								</tr>
								<tr>
									<td>End of tenancy cleaning</td>
									<td>$25 - $200</td>
								</tr>
								<tr>
									<td>House cleaning</td>
									<td>$125</td>
								</tr>
								<tr>
									<td>Upholstery cleaning</td>
									<td>From $40 / item</td>
								</tr>
								<tr>
									<td>Domestic cleaning</td>
									<td>From $75 / day</td>
								</tr>
								<tr>
									<td>Office cleaning</td>
									<td>$199 per cleaning</td>
								</tr>
								<tr>
									<td>Other cleaning</td>
									<td>$125 - $275</td>
								</tr>
							</tbody>
						</table>
					</div-->
				</div>
			</div>
		</div>
	</div>
</div>