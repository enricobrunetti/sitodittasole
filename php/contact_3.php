	<div class="row gray full-width page-header vertical-align-table">
		<div class="row">
			<div class="page-header-left">
				<h1>CONTATTACI</h1>
			</div>
			<div class="page-header-right">
				<div class="bread-crumb-container">
					<ul class="bread-crumb">
						<li>
							<a title="Home" href="?page=home">
								Home
							</a>
						</li>
						<li class="separator">
							&#47;
						</li>
						<li>
							Contatti
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="theme-page">
	<div class="row full-width header-background header-background-4"></div>
	<div class="clearfix" style="margin-bottom: 50px;">
		<div class="row page-margin-top-section">
			<ul class="features-list clearfix">
				<li class="column column-1-4">
					<div class="icon features-map"></div>
					<h4>MOBILITÀ</h4>
					<p>Operiamo in tutta<br>la Romagna.</p>
				</li>
				<li class="column column-1-4">
					<div class="icon features-phone"></div>
					<h4>TELEFONO</h4>
					<p>Cell: <a href="tel:+393549563776">+39 354 9563776</a><br>Cell: <a href="tel:+393358368978">+39 335 8368978</a></p>
				</li>
				<li class="column column-1-4">
					<div class="icon features-email"></div>
					<h4>EMAIL</h4>
					<p><a href="mailto:a.solepulizie@gmail.com">a.solepulizie@gmail.com</a></p>
				</li>
				<li class="column column-1-4">
					<div class="icon features-clock"></div>
					<h4>ORARI</h4>
					<p>Siamo aperti tutti i giorni<br>in base alle tue esigenze</p>
				</li>
			</ul>
		</div>
		<div class="row margin-top-67">
			<form class="contact-form" id="contact-form" method="post" action="contact_form/contact_form.php">
				<div class="row flex-box">
					<fieldset class="column column-1-2">
						<label>NOME E COGNOME</label>
						<input class="text-input" name="name" type="text" value="">
						<label>EMAIL</label>
						<input class="text-input" name="email" type="text" value="">
						<label>TELEFONO</label>
						<input class="text-input" name="phone" type="text" value="">
					</fieldset>
					<fieldset class="column column-1-2">
						<label>MESSAGGIO</label>
						<textarea name="message"></textarea>
					</fieldset>
				</div>
				<div class="row margin-top-30">
					<div class="column column-1-1">
						<input type="hidden" name="action" value="contact_form" />
						<div class="row margin-top-15 padding-bottom-16 align-center">
							<a class="more submit-contact-form" href="#" title="Send message">Invia messaggio</a>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>