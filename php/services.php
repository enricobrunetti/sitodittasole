<div class="theme-page padding-bottom-100">
	<div class="row gray full-width page-header vertical-align-table">
		<div class="row">
			<div class="page-header-left">
				<h1>I NOSTRI SERVIZI</h1>
			</div>
			<div class="page-header-right">
				<div class="bread-crumb-container">
					<ul class="bread-crumb">
						<li>
							<a title="Home" href="?page=home">
								Home
							</a>
						</li>
						<li class="separator">
							&#47;
						</li>
						<li>
							Servizi
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<div class="clearfix">
		<div class="row page-margin-top-section">
			<ul class="services-list gray clearfix">
				<li>
					<a href="?page=service_move_in_out" title="Move In Out Service">
						<img src="images/moquette.jpg" alt="">
					</a>
					<h4 class="box-header"><a href="?page=service_move_in_out" title="Move In Out Service">LAVAGGIO DI MOQUETTE</a></h4>
					<p>Utilizziamo protocolli specifici e macchinari ad iniezione e aspirazione per rendere la tua moquette come il primo giorno.</p>
				</li>
				<li>
					<a href="?page=service_commercial_cleaning" title="Commercial Cleaning">
						<img src="images/divano.jpg" alt="">
					</a>
					<h4 class="box-header"><a href="?page=service_commercial_cleaning" title="Commercial Cleaning">LAVAGGIO INTEGRALE DIVANI</a></h4>
					<p>Rivitalizziamo i tessuti con macchinari specifici ed altamente professionali, adatti alle aree non sfoderabili ed imbottite.</p>
				</li>
				<li>
					<a href="?page=service_window_cleaning" title="Window Cleaning">
						<img src="images/vetrata.jpg" alt="">
					</a>
					<h4 class="box-header"><a href="?page=service_window_cleaning" title="Window Cleaning">LAVAGGIO INTEGRALE VETRATE</a></h4>
					<p>Disponiamo dei prodotti migliori per il trattamento completo dei vetri ed eliminiamo lo sporco più ostinato con olio di gomito.</p>
				</li>
				<li>
					<a href="?page=service_pulizia_palestre" title="Pulizia Palestre">
						<img src="images/palestra.jpg" alt="">
					</a>
					<h4 class="box-header"><a href="?page=service_pulizia_palestre" title="Pulizia Palestre">PULIZIA COMPLETA PALESTRE</a></h4>
					<p>Ci occupiamo della pulizia di ogni ambiente della palestra, nel rispetto delle condizioni igienico-sanitarie richieste da tali spazi.</p>
				</li>
				<li>
					<a href="?page=service_pulizia_negozi" title="Pulizia Negozi">
						<img src="images/negozio.jpg" alt="">
					</a>
					<h4 class="box-header"><a href="?page=service_pulizia_negozi" title="Pulizia Negozi">PULIZIA COMPLETA NEGOZI</a></h4>
					<p>Curiamo l’immagine della tua realtà al fine di assicurare la buona rendita della tua attività e la fidelizzazione della tua clientela.</p>
				</li>
				<li>
					<a href="?page=service_house_cleaning" title="House Cleaning">
						<img src="images/ufficio.jpg" alt="">
					</a>
					<h4 class="box-header"><a href="?page=service_house_cleaning" title="House Cleaning">PULIZIA PERIODICA DI UFFICI</a></h4>
					<p>Offriamo singoli, periodici o continuativi servizi di sanificazione all’interno di uffici privati e pubblici o generici ambienti lavorativi.</p>
				</li>
				<li>
					<a href="?page=service_post_renovation" title="Post Renovation">
						<img src="images/appartamento.jpg" alt="">
					</a>
					<h4 class="box-header"><a href="?page=service_post_renovation" title="Post Renovation">PULIZIA DI APPARTAMENTI</a></h4>
					<p>Garantiamo sempre un accurato servizio di ripristino o mantenimento all’interno di ogni tipo di appartamento privato.</p>
				</li>
				<li>
					<a href="?page=service_green_spaces_maintenance" title="Green Spaces Maintenance">
						<img src="images/pavimento.jpg" alt="">
					</a>
					<h4 class="box-header"><a href="?page=service_green_spaces_maintenance" title="Green Spaces Maintenance">RIPRISTINO PAVIMENTAZIONE</a></h4>
					<p>Uno dei servizi più apprezzati offerti dalla nostra ditta. Trattiamo ogni tipo di superficie: dal cotto al marmo, fino al parquet.</p>
				</li>
			</ul>
		</div>
	</div>
</div>